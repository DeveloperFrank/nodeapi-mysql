'use strict';

module.exports.query = {

	app.get('/api/v1/countrys', function(req, res, next) {
		connection.query("SELECT Name, Continent, Region, SurfaceArea, IndepYear, Population, LifeExpectancy, HeadOfState"+
						 " FROM country", function(error, rows) {
			//connection.release();
			if(!!error) {
				console.log("Error en la consulta");
			}
			else{
				console.log("Consulta realizada, Paises: ");
				console.log(req.headers);
				res.json(rows);
				console.log("**********************************************************************************");
			}
		});
	});

	app.get('/api/v1/citys', function(req, res) {
		connection.query("SELECT * FROM city", function(error, rows) {
			//connection.release();
			if(!!error) {
				console.log("Error en la consulta");
			}
			else{
				console.log("Consulta realizada, Ciudades: ");
				console.log(req.headers);
				res.json(rows);
				console.log("**********************************************************************************");
			}
		});
	});

	app.get('/api/v1/language', function(req, res) {
		connection.query("SELECT * FROM countrylanguage", function(error, rows) {
			//connection.release();
			if(!!error) {
				console.log("Error en la consulta");
			}
			else{
				console.log("Consulta realizada, Lenguajes, paises: ");
				console.log(req.headers);
				res.json(rows);
				console.log("**********************************************************************************");
			}
		});
	});

	app.get('/api/v1/l_country', function(req, res) {
		connection.query("SELECT country.Name, " +
								 "countrylanguage.Language, countrylanguage.Percentage " +
								 "FROM country join countrylanguage "+
								 	"on country.Code = countrylanguage.CountryCode "+
								 		"order by country.Name", function(error, rows) {
			//connection.release();
			if(!!error) {
				console.log("Error en la consulta");
			}
			else{
				console.log("Consulta lenguajes por paises: ");
				console.log(req.headers);
				res.json(rows);
				console.log("**********************************************************************************");
			}
		});
	});

	//Población por continente
	app.get('/api/v1/continent_population', function(req, res) {
		connection.query("SELECT Continent, sum(Population) as total FROM country GROUP by Continent", function(error, rows) {
			//connection.release();
			if(!!error) {
				console.log("Error en la consulta");
			}
			else{
				console.log("Consulta lenguajes por paises: ");
				console.log(req.headers);
				res.json(rows);
				console.log("**********************************************************************************");
			}
		});
	});
}