//node modules
var express = require('express');
var mysql = require('mysql');
var app = express();

//especificación de los metodos permitidos por la api (GET->solo lectura de datos.) metodo para CORS
var allowMethods = function(req, res, next){
	res.header('Access-Control-Allow-Origin', req.headers.origin || "*");
	res.header('Access-Control-Allow-Methods', 'GET');
	next();
}

//Seguridad de la Api V1, se establece contraseña.
var auth = function(req, res, next) {
	if(req.headers.token === "api_V1_paises") {
		return next();
	}
	else {
		return next(new Error('Acceso no autorizado'));
	}
}


//Conexión con la base de datos
var connection = mysql.createConnection({
	host: 'localhost',
	user: 'root',
	password: '',
	database: 'world'
});

//se crea el enlace con la base de datos MySql
connection.connect(function(error) {
	if(!!error){
		console.log('error!');
	}
	else{
		console.log('-------------------------------------------------------------------');
		console.log(' Se a conectado correctamente al servicio de ApiRest V1.0 Paises.');
		console.log('-------------------------------------------------------------------');
	}
});

//Se establece la configuracion de los parametros permitidos por petición.
app.use(allowMethods);

app.get('/api/v1/countrys', function(req, res, next) {
	connection.query("SELECT Name, Continent, Region, SurfaceArea, IndepYear, Population, LifeExpectancy, HeadOfState"+
					 " FROM country", function(error, rows) {
		//connection.release();
		if(!!error) {
			console.log("Error en la consulta");
		}
		else{
			console.log("Consulta realizada, Paises: ");
			console.log(req.headers);
			res.json(rows);
			console.log("**********************************************************************************");
		}
	});
});

app.get('/api/v1/citys', function(req, res) {
	connection.query("SELECT * FROM city", function(error, rows) {
		//connection.release();
		if(!!error) {
			console.log("Error en la consulta");
		}
		else{
			console.log("Consulta realizada, Ciudades: ");
			console.log(req.headers);
			res.json(rows);
			console.log("**********************************************************************************");
		}
	});
});

app.get('/api/v1/language', function(req, res) {
	connection.query("SELECT * FROM countrylanguage", function(error, rows) {
		//connection.release();
		if(!!error) {
			console.log("Error en la consulta");
		}
		else{
			console.log("Consulta realizada, Lenguajes, paises: ");
			console.log(req.headers);
			res.json(rows);
			console.log("**********************************************************************************");
		}
	});
});

app.get('/api/v1/l_country', function(req, res) {
	connection.query("SELECT country.Name, " +
							 "countrylanguage.Language, countrylanguage.Percentage " +
							 "FROM country join countrylanguage "+
							 	"on country.Code = countrylanguage.CountryCode "+
							 		"order by country.Name", function(error, rows) {
		//connection.release();
		if(!!error) {
			console.log("Error en la consulta");
		}
		else{
			console.log("Consulta lenguajes por paises: ");
			console.log(req.headers);
			res.json(rows);
			console.log("**********************************************************************************");
		}
	});
});

//Población por continente
app.get('/api/v1/continent_population', function(req, res) {
	connection.query("SELECT Continent, sum(Population) as total FROM country GROUP by Continent", function(error, rows) {
		//connection.release();
		if(!!error) {
			console.log("Error en la consulta");
		}
		else{
			console.log("Consulta lenguajes por paises: ");
			console.log(req.headers);
			res.json(rows);
			console.log("**********************************************************************************");
		}
	});
});

app.listen(1337);